public class MaterialPoint2D extends Point2D {

    private double mass;

    public MaterialPoint2D(double x, double y, double mass) {
        super(x,y);
        this.mass = mass;
    }

    @Override
    public String toString() {
        return "x = "+getX()+", y = "+getY()+", mass = "+mass;
    }

    @Override
    double getX() {
        return super.getX();
    }

    @Override
    double getY() {
        return super.getY();
    }
    public double getMass() {
        return mass;
    }
}
