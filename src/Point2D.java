public class Point2D {

    private double x,y;

    public Point2D(double x, double y)
    {
        this.x = x;
        this.y = y;
    }
    double getX() {
        return x;
    }
    double getY() {
        return y;
    }

    @Override
    public String toString() {
        return "x = " + x +
                ", y = " + y;
    }
}
