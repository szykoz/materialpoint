public class Calculations {

    public static Point2D positionGeometricCenter(Point2D[] point) {

        double sumX=0;
        double sumY=0;
        int size=0;

        for (Point2D p: point)
        {
            sumX += p.getX();
            sumY += p.getY();
            size ++;
        }

        return new Point2D(sumX/size,sumY/size);
    }

    public static Point2D positionCenterOfMass (MaterialPoint2D[] materialPoint) {

        double sumX=0;
        double sumY=0;
        double sumMass=0;
        for (MaterialPoint2D p: materialPoint)
        {
            sumX += p.getX()*p.getMass();
            sumY += p.getY()*p.getMass();
            sumMass += p.getMass();
        }
        return new MaterialPoint2D(sumX/sumMass,sumY/sumMass,sumMass);

    }
}
